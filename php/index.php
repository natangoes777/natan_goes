<?php
$url = "https://pokeapi.co/api/v2/pokemon?limit=50";
$resposta = json_decode(file_get_contents($url));
$pokemons = $resposta->results;
?>

<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <title>Pokedex</title>
    </head>
    <body>

        <div class="header">

            <img class="header-logo" src="../img/pokebola-go.png" title="Transparência Traduzida" alt="">
            <img class="header-title" src="../img/pokemon-png-logo.png" alt="">

        </div>

        <div class="main">

            <?php
                for($i = 0; $i < 50; $i++){
                    $infos = json_decode(file_get_contents($pokemons[$i]->url));

                    $nome = $infos->name;
                    $imagem = $infos->sprites->other->dream_world->front_default;

                    $habilidades = $infos->abilities;
                    $habilidade1 = $habilidades[0]->ability->name;
                    $habilidade2 = $habilidades[1]->ability->name;

                    $tipos = $infos->types;
                    $tipo1 = $tipos[0]->type->name;
                    $tipo2 = $tipos[1]->type->name;

                echo '<div class="item">

                        <div class="item_img">
                            <img src="'.$imagem.'"  alt="">
                        </div>
                        
                        <div class="item_info">
                            <h1 class="item_name">'.$nome.'</h1>
                            
                            <div class="item_type">
                                <span>Type</span>
        
                                <ul class="type_list">
                                    <li>
                                        <a>'.$tipo1.'</a>
                                    </li>';
                                    if($tipo2 != null)
                                        echo '<li>
                                                  <a>'.$tipo2.'</a>
                                              </li>';
                          echo '</ul>
                            </div>
                            
                            <div class="item_abilitys">
                                <span> Abilitys </span>
            
                                <ul class="abilitys_list">
                                    <li>
                                        <a>'.$habilidade1.'</a>
                                    </li>';
                                    if($habilidade2 != null)
                                        echo '<li>
                                                  <a>'.$habilidade2.'</a>
                                              </li>';
                          echo '</ul>
                            </div>
                        </div>
        
                    </div>';

                }
            ?>

        </div>

   </body>
</html>